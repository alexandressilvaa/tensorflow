import 'package:flutter/material.dart';

const primaryColor = Color(0xff3e8df4);
const secondaryColor = Color(0xff47bcfb);
const bgColor = Color(0xff101820);
const bgWhite = Color(0xffffffff);

const defaultPadding = 16.0;
