import 'package:flutter/material.dart';
import 'package:tensorflow_hoobox/screen/frame/components/box_widget.dart';
import 'package:tensorflow_hoobox/screen/frame/components/camera_view.dart';
import 'package:tensorflow_hoobox/models/tflite/recognition.dart';
import 'package:tensorflow_hoobox/models/tflite/stats.dart';

class FrameScreen extends StatefulWidget {

  @override
  _FrameScreenState createState() => _FrameScreenState();
}

class _FrameScreenState extends State<FrameScreen> {

  List<Recognition> results;

  Stats stats;

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(left: 25, right: 25, bottom: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: 54,
              child: ElevatedButton(
                onPressed: Navigator.of(context).pop,
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
                ),
                child: Text('VOLTAR'),
              ),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: Stack(
        children: <Widget>[
          // Camera
          CameraView(resultsCallback, statsCallback),

          // Resultado Frame
          boundingBoxes(results),

        ],
      ),
    );
  }

  Widget boundingBoxes(List<Recognition> results) {
    if (results == null) {
      return Container();
    }
    return Stack(
      children: results
          .map((e) => BoxWidget(
        result: e,
      )).toList(),
    );
  }

  void resultsCallback(List<Recognition> results) {
    if(mounted)setState(() {
      this.results = results;
    });
  }

  void statsCallback(Stats stats) {
    if(mounted)setState(() {
      this.stats = stats;
    });
  }
}

class StatsRow extends StatelessWidget {
  final String left;
  final String right;

  StatsRow(this.left, this.right);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [Text(left), Text(right)],
      ),
    );
  }
}
