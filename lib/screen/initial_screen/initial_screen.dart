import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:tensorflow_hoobox/screen/home/home_screen.dart';

class InitialScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SplashScreen(
          seconds: 4,
          useLoader: false,
          navigateAfterSeconds: HomeScreen(),
          backgroundColor: Color(0xff1d293f),
        ),
        Center(
          child: Container(
            width: 100,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/logo_hoobox.png')
              )
            ),
          ),
        )
      ],
    );
  }
}
