import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tensorflow_hoobox/common/constants.dart';
import 'package:tensorflow_hoobox/screen/frame/frame_screen.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [secondaryColor, primaryColor]),
      ),
      child: Scaffold(
        bottomNavigationBar: SizedBox(
          height: 70,
          child: Card(
            color: bgWhite,
            margin: EdgeInsets.zero,
            elevation: 4,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
            child: Padding(
              padding: const EdgeInsets.only(left: 55, right: 40),
              child: Row(
                children: [
                  Container(
                    height: 47,
                    width: 34,
                    child: Image.asset('assets/p.png'),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Container(
                        width: 100,
                        height: 65,
                        child: Image.asset('assets/home.png'),
                      ),
                    ),
                  ),
                  Container(
                    width: 62,
                    height: 47,
                    child: Image.asset('assets/h.png'),
                  )
                ],
              ),
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Container(
              height: 138,
              child: Padding(
                padding: const EdgeInsets.all(25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 15,
                      child: Image.asset('assets/drawer.png'),
                    ),
                    Row(
                      children: [
                        Text('Maria Aparecida', style: TextStyle(color: bgWhite, fontSize: 16)),
                        Padding(
                          padding: const EdgeInsets.only(left: 5),
                          child: Icon(Icons.keyboard_arrow_down, size: 15, color: bgWhite,),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 5),
                          height: 30,
                          child: Image.asset('assets/perfil.png'),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 120),
              decoration: BoxDecoration(
                color: bgWhite,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30))
              ),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 44, left: 25),
                    child: Row(
                      children: [
                        Text('Olá, ', style: TextStyle(color: bgColor, fontSize: 32)),
                        Text('Maria!', style: TextStyle(color: secondaryColor, fontSize: 32, fontWeight: FontWeight.w600))
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (_)=>FrameScreen()));
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 58, left: 10, right: 10),
                      width: MediaQuery.of(context).size.width,
                      height: 185,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/img1.png'),
                          fit: BoxFit.fill
                        )
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
