import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tensorflow_hoobox/screen/initial_screen/initial_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'TensorFlow Hoobox',
      theme: ThemeData.dark().copyWith(
        primaryColor: Color(0xff3e8df4),
        scaffoldBackgroundColor: Colors.white,
        textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
            .apply(bodyColor: Colors.black),
        canvasColor: Colors.transparent,
      ),
      home: InitialScreen(),
    );
  }
}