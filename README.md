# TensorFlow Hoobox

### Autor

 - Desenvolvedor: Alexandre Silva
 - Email: alexandrebmx5@gmail.com

#### Frameworks

 - [Dart 2.13.0](https://dart.dev/)
 - [Flutter 2.2.0](https://flutter.dev/)


## (Importante) Configuração inicial: adicione bibliotecas dinâmicas ao seu aplicativo

### Android

1. Coloque o script [install.sh](https://github.com/am15h/tflite_flutter_plugin/blob/master/install.sh) (Linux/Mac) ou [install.bat](https://github.com/am15h/tflite_flutter_plugin/blob/master/install.bat) (Windows) na raiz do seu projeto.

2. Execute `sh install.sh` (Linux) / `install.bat` (Windows) a raiz do seu projeto para baixar automaticamente e colocar os binários nas pastas apropriadas.

Esses scripts instalam binários pré-construídos com base na versão mais recente do tensorflow estável. Para obter informações sobre como usar outras versões do tensorflow, siga [instruções no wiki](https://github.com/am15h/tflite_flutter_plugin/wiki/). 

#### Organização do codigo

1. Pasta assets, encontra-se os arquivos de configuração do tensorflow, labels e as imgs do projeto.

2. Pasta models/tflite, encontra-se os arquivos de carregamentos dos modulos e os rótulos.

3. Pasta utils, encontra-se os arquivos de conversão de camera-imagem em imagem.

4. Pasta screen, encontra-se as telas de front end e os componentes que pertence as telas.

5. Pasta common, encontra-se o arquivo de configuração das cores do app.

## Execução do projeto

#### git clone

```
git clone https://gitlab.com/alexandressilvaa/tensorflow.git
```

#### Install 

```
flutter packages get
```

#### Run

```
flutter run
```

#### Aplicativo para teste
[Apk](app.apk)


